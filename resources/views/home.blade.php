@extends('layouts.app')

@section('title-block')Главная@endsection

@section('content')
    <h1>Главная страница</h1>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis consequatur, perspiciatis accusantium omnis, necessitatibus dicta ipsam provident nihil doloribus adipisci obcaecati consectetur reiciendis! Sed possimus, accusantium eum ea exercitationem totam!</p>
@endsection

@section('aside')
    @parent
    <p>Дополнительный текст</p>
@endsection
