@extends('layouts.app')

@section('title-block')Сообщение@endsection

@section('content')
    <h1>Сообщение от {{ $data->name }}</h1>
    
    <div class="alert alert-info">
        <h3> {{ $data->message }} </h3>
        <p> {{ $data->email }} </p>
        <p><small> {{ $data->created_at }} </small></p>
        <a href="{{ route('contact-update', $data->id) }}"><button class="btn btn-warning">Редактировать</button></a>
        <a href="{{ route('contact-delete', $data->id) }}"><button class="btn btn-danger">Удалить</button></a>
    </div>
@endsection

