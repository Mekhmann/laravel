@extends('layouts.app')

@section('title-block')Страница контактов@endsection

@section('content')
    <h1>Контакты</h1>

    <form action="{{ route('contact-form') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Введите имя</label>
            <input type="text" name="name" placeholder="Введите имя" id="name" class="form-control">
        </div>

        <div class="form-group mt-3">
            <label for="email">Email</label>
            <input type="text" name="email" placeholder="Введите email" id="email" class="form-control">
        </div>

        <div class="form-group mt-3">
            <label for="message">Сообщение</label>
            <textarea name="message" id="message" class="form-control" placeholder="Введите сообщения"></textarea>
        </div>

        <button type="submit" class="btn btn-success mt-3">Отправить</button>
    </form>
@endsection